package consCl;
import java.sql.SQLException;

import java.util.Scanner;


public class Ejecutable {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
		baseDatos bdatos = new baseDatos();
				//INICIO SCANNER
		Scanner scanner = new Scanner(System.in);
		
		boolean salir = false;
        int opcion;
      

        Scanner sc = new Scanner(System.in);
        
        /*Bucle que permite repetir el menu*/
        while (!salir) {
            System.out.println("Men� Principal ");
            System.out.println("Ingrese la opci�n deseada: ");
            System.out.println("1 - Ingresar nuevo cliente ");
            System.out.println("2 - Ver listado de clientes: ");
            System.out.println("3 - Buscar clientes por su DNI ");
            System.out.println("4 - SALIR ");
            opcion = sc.nextInt();

            switch (opcion) {
            //AGREGAR CLIENTE
                case 1:
                    System.out.println("AGREGAR NUEVO CLIENTE ");
                    System.out.println("Ingrese el nombre del cliente: ");
            		String clNombre = scanner.nextLine();	
            		System.out.println("Ingrese el apellido del cliente: ");
            		String clApellido = scanner.nextLine();
            		System.out.println("Ingrese el mail del cliente: ");
            		String clMail = scanner.nextLine();
            		System.out.println("Ingrese monto: ");
            		int clMonto = scanner.nextInt();
            		System.out.println("Ingrese dni: ");
            		int clDni= scanner.nextInt();
            		System.out.println("Ingrese n�mero de cuenta: ");
            		int clNroCuenta= scanner.nextInt();
            		Clientes cl1 = new Clientes (clNombre, clApellido, clMail, clMonto, clDni, clNroCuenta);
            		System.out.println(" ");
            		System.out.println("Datos Ingresados: ");
            		System.out.println("Nombre: " + cl1.getNombre());
            		System.out.println( "Apellido: " + cl1.getApellido());
            		System.out.println("Mail: " + cl1.getMail());
            		System.out.println("Monto: " + cl1.getMonto() );
            		System.out.println("Dni: " + cl1.getDni());
            		System.out.println("Nro de Cuenta: " + cl1.getNroCuenta());;
            		//Agregar CLIENTE A LA BD
            		bdatos.agregarALaBD(cl1.getNombre(), cl1.getApellido(), cl1.getMail(), cl1.getMonto(), cl1.getDni(), cl1.getNroCuenta());       
                    break;
                    	
                
                    //VER LISTADO DE CLIENTES
                case 2:
                    System.out.println("LISTADO DE CLIENTES: ");
                    bdatos.mostrarClientes();
                    break;
                    //BUSCAR CLIENTE POR SU DNI
                case 3:
                    System.out.println("BUSCAR CLIENTES POR SU DNI");
                    System.out.println("Ingrese el DNI del cliente que desea buscar: ");
            		int clxDNI = scanner.nextInt();
            		bdatos.mostrarClientesPORDNI(clxDNI);
                    break;
                    
                case 4:
                	salir = true;
                    System.out.print("Programa finalizado");
                    break;
                default:
                    System.out.print("Opcion no valida!");
            }
        }
		
		
				
	}

}
