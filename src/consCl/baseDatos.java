package consCl;

//add
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.*;

public class baseDatos {

//INSERT A CLIENTES
	public static void agregarALaBD(String nombre, String apellido, String mail, int monto, int dni, int nrocuenta)
				throws SQLException, ClassNotFoundException {
			try {
				String myDriver = "org.gjt.mm.mysql.Driver";
				String myUrl = "jdbc:mysql://localhost/consultacliente"; //nombreMIbd
				Class.forName(myDriver);

				Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
				
				String query = " insert into clientes (nombre, apellido, mail, monto, dni, nrocuenta)" + " values (?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStmt = conn.prepareStatement(query);
				preparedStmt.setString(1, nombre);
				preparedStmt.setString(2, apellido);
				preparedStmt.setString(3, mail);
				preparedStmt.setInt(4, monto);
				preparedStmt.setInt(5, dni);
				preparedStmt.setInt(6, nrocuenta);
				
				preparedStmt.executeUpdate();

				
				conn.close();
			} catch (SQLException se) {
				// log the exception
				throw se;
			}
		}
		

//MOSTRAR LISTADO DE CLIENTES
	public static void mostrarClientes()
			throws SQLException, ClassNotFoundException {
		 try
	        {
			 String myDriver = "org.gjt.mm.mysql.Driver";
				String myUrl = "jdbc:mysql://localhost/consultacliente"; //nombreMIbd
				Class.forName(myDriver);
				Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
	            
	            Statement s = conn.createStatement();
	            
	            // Se realiza la consulta. Los resultados se guardan en el 
	            // ResultSet rs
	            ResultSet rs = s.executeQuery ("select * from Clientes");
	            
	            // Se recorre el ResultSet, mostrando por pantalla los resultados.
	            while (rs.next())
	            {
	                System.out.println ("Cliente: " + rs.getInt ("Id") + " | " + 
	            "Nombre: " + rs.getString ("Nombre")+  " | " + 
	             "Apellido: " + rs.getString("Apellido")+ " | " + 
	            "Mail: "+ rs.getString("Mail")+ " " + 
	             "Monto: " + rs.getInt("Monto")+ " | " + 
	            "DNI: " + rs.getInt("DNI")+ " | " + 
	             "Nro Cuenta: " + rs.getInt("NroCuenta")+ " | ");
	            }
	            
	            conn.close();
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
}
	
	
//MOSTRAR CLIENTES X DNI
public static void mostrarClientesPORDNI(int dni)
				throws SQLException, ClassNotFoundException {
			 try
		        {
				 String myDriver = "org.gjt.mm.mysql.Driver";
					String myUrl = "jdbc:mysql://localhost/consultacliente"; //nombreMIbd
					Class.forName(myDriver);
					Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
		            
		            Statement s = conn.createStatement();
		            
		            // Se realiza la consulta. Los resultados se guardan en el 
		            // ResultSet rs
		            ResultSet rs = s.executeQuery ("select * from Clientes WHERE dni= "+ dni );
		            
		            // Se recorre el ResultSet, mostrando por pantalla los resultados.
		            while (rs.next())
		            {
		                System.out.println ("Cliente: " + rs.getInt ("Id") + " | " + 
		            "Nombre: " + rs.getString ("Nombre")+  " | " + 
		             "Apellido: " + rs.getString("Apellido")+ " | " + 
		            "Mail: "+ rs.getString("Mail")+ " " + 
		             "Monto: " + rs.getInt("Monto")+ " | " + 
		            "DNI: " + rs.getInt("DNI")+ " | " + 
		             "Nro Cuenta: " + rs.getInt("NroCuenta")+ " | ");
		            }
		            
		            conn.close();
		        }
		        catch (Exception e)
		        {
		            e.printStackTrace();
		        }
	}
	
	
	
}
